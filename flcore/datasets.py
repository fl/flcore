import bz2
import os
import shutil
import urllib.request
from typing import Tuple

import numpy as np
import openml
import pandas as pd
from sklearn.datasets import load_svmlight_file
from sklearn.preprocessing import LabelEncoder, MinMaxScaler,StandardScaler
from sklearn.model_selection import KFold, StratifiedShuffleSplit, train_test_split
from sklearn.utils import shuffle

from flcore.models.xgb.utils import TreeDataset, do_fl_partitioning, get_dataloader

XY = Tuple[np.ndarray, np.ndarray]
Dataset = Tuple[XY, XY]


def load_mnist(center_id=None, num_splits=5):
    """Loads the MNIST dataset using OpenML.
    OpenML dataset link: https://www.openml.org/d/554
    """
    mnist_openml = openml.datasets.get_dataset(554)
    Xy, _, _, _ = mnist_openml.get_data(dataset_format="array")
    X = Xy[:, :-1]  # the last column contains labels
    y = Xy[:, -1]
    # print(X.shape)
    # print(y.shape)
    # print(y[0])
    # First 60000 samples consist of the train set
    # x_train, y_train = X[:60000], y[:60000]
    # x_train, y_train = X[:1000], y[:1000]
    # # x_test, y_test = X[60000:], y[60000:]
    # x_test, y_test = X[1000:], y[1000:]
    x_train = X
    y_train = y

    if center_id != None:
        # Split the data
        kf = KFold(n_splits=num_splits, shuffle=True, random_state=42)
        for i, (train_index, test_index) in enumerate(kf.split(X)):
            if i + 1 != center_id:
                continue
            x_train, y_train = X[train_index], y[train_index]
            x_train, x_test, y_train, y_test = train_test_split(
                x_train, y_train, test_size=0.2, random_state=42
            )
            print(f"Loaded subset of MNIST with fold {i+1} out of {num_splits}.")
    else:
        x_train, y_train = X[:60000], y[:60000]
        x_test, y_test = X[60000:], y[60000:]

    # y_train = np.array(np.array(y_train, dtype=bool), dtype=float)
    # y_test = np.array(np.array(y_test, dtype=bool), dtype=float)
    x_train = x_train[:1000]
    y_train = y_train[:1000]
    x_test = x_test[:1000]
    y_test = y_test[:1000]

    return (x_train, y_train), (x_test, y_test)


def load_cvd(data_path, center_id=None) -> Dataset:
    id = center_id
    # match num_center:
    #     case -1:
    #         file_name = data_path+'data_centerAll.csv'
    #     case 1:
    #         file_name = data_path+'data_center1.csv'
    #     case 2:
    #         file_name = data_path+'data_center2.csv'
    #     case _:
    #         file_name = data_path+'data_center3.csv'
    #
    if id == None:
        # id = 'All'
        data_centers = ['All']
    else:
        data_centers = [id]

    X_train_list, y_train_list = [], []
    X_test_list, y_test_list = [], []
    test_index_list = []
    train_index_list = []

    for id in data_centers:
        file_name = os.path.join(data_path, f"data_center{id}.csv")

        code_id = "f_eid"
        code_outcome = "Eval"

        data = pd.read_csv(file_name)
        X_data = data.drop([code_id, code_outcome], axis=1)
        y_data = data[code_outcome]
        f_eid = data[code_id]

        # Split the data
        sss = StratifiedShuffleSplit(n_splits=1, test_size=0.2, random_state=42)
        train_index, test_index = next(sss.split(X_data, y_data))
        X_test = X_data.iloc[test_index, :]
        X_train = X_data.iloc[train_index, :]
        y_test, y_train = y_data.iloc[test_index], y_data.iloc[train_index]
        # We save the names
        f_eid.iloc[test_index]
        f_eid.iloc[train_index]

        X_train_list.append(X_train)
        y_train_list.append(y_train)
        X_test_list.append(X_test)
        y_test_list.append(y_test)
        train_index_list.append(train_index)
        test_index_list.append(test_index)

    X_train = pd.concat(X_train_list)
    y_train = pd.concat(y_train_list)
    X_test = pd.concat(X_test_list)
    y_test = pd.concat(y_test_list)
    train_index = np.concatenate(train_index_list)
    test_index = np.concatenate(test_index_list)

    # Verify set difference, data centers overlap
    # print(len(train_index.tolist()))
    # print(len(test_index.tolist()))
    # train_set = set(train_index.tolist())
    # test_set = set(test_index.tolist())
    # diff = train_set.intersection(test_set)
    # print(len(train_set))
    # print(len(test_set))
    # print( len(diff) )
    # print(f"SUBSET {id}")
    # train_unique = np.unique(y_train, return_counts=True)
    # test_unique = np.unique(y_test, return_counts=True)
    # train_max_acc = train_unique[1][0]/len(y_train)
    # test_max_acc = test_unique[1][0]/len(y_test)
    # print(np.unique(y_train, return_counts=True))
    # print(np.unique(y_test, return_counts=True))
    # print(train_max_acc)
    # print(test_max_acc)

    return (X_train, y_train), (X_test, y_test)


def load_kaggle_hf(data_path, center_id=None) -> Dataset:
    id = center_id
    
    if id == 1:
        id = 'switzerland'
    elif id == 2:
        id = 'hungarian'
    elif id == 3:
        id = 'va'
    elif id == 4:
        id = 'cleveland'
    elif id == 5:
        id = 'cleveland'

    file_name = os.path.join(data_path, "kaggle_hf.csv")
    data = pd.read_csv(file_name)
    if id is not None:
        data = data.loc[(data['data_center'] == id)]

    col = list(data.columns)
    categorical_features = []
    numerical_features = []
    for i in col:
        if len(data[i].unique()) > 6:
            numerical_features.append(i)
        else:
            categorical_features.append(i)

    # print('Categorical Features :',*categorical_features)
    # print('Numerical Features :',*numerical_features)

    le = LabelEncoder()
    df1 = data.copy(deep = True)

    df1['Sex'] = le.fit_transform(df1['Sex'])
    df1['ChestPainType'] = le.fit_transform(df1['ChestPainType'])
    df1['RestingECG'] = le.fit_transform(df1['RestingECG'])
    df1['ExerciseAngina'] = le.fit_transform(df1['ExerciseAngina'])
    df1['ST_Slope'] = le.fit_transform(df1['ST_Slope'])

    
    # mms = MinMaxScaler() # Normalization
    # ss = StandardScaler() # Standardization
    # df1['Oldpeak'] = mms.fit_transform(df1[['Oldpeak']])
    # df1['Age'] = ss.fit_transform(df1[['Age']])
    # df1['RestingBP'] = ss.fit_transform(df1[['RestingBP']])
    # df1['Cholesterol'] = ss.fit_transform(df1[['Cholesterol']])
    # df1['MaxHR'] = ss.fit_transform(df1[['MaxHR']])

    # features = df1[df1.columns.drop(['HeartDisease','RestingBP','RestingECG', 'data_center'])].values
    # target = df1['HeartDisease'].values
    features = df1[df1.columns.drop(['HeartDisease','RestingBP','RestingECG', 'data_center'])]
    target = df1['HeartDisease']
    X_train, X_test, y_train, y_test = train_test_split(features, target, test_size = 0.20, random_state = 2)

    mms = MinMaxScaler() # Normalization
    ss = StandardScaler() # Standardization

    X_train['Oldpeak'] = mms.fit_transform(X_train[['Oldpeak']])
    X_test['Oldpeak'] = mms.transform(X_test[['Oldpeak']])

    X_train['Age'] = ss.fit_transform(X_train[['Age']])
    X_test['Age'] = ss.transform(X_test[['Age']])

    # X_train['RestingBP'] = ss.fit_transform(X_train[['RestingBP']])
    # X_test['RestingBP'] = ss.transform(X_test[['RestingBP']])

    X_train['Cholesterol'] = ss.fit_transform(X_train[['Cholesterol']])
    X_test['Cholesterol'] = ss.transform(X_test[['Cholesterol']])

    X_train['MaxHR'] = ss.fit_transform(X_train[['MaxHR']])
    X_test['MaxHR'] = ss.transform(X_test[['MaxHR']])
    
    # print(X_train.shape)
    # print(y_train.shape)
    # print(X_test.shape)
    # print(y_test.shape)
    return (X_train, y_train), (X_test, y_test)


def load_libsvm(config, center_id=None, task_type="BINARY"):
    # ## Manually download and load the tabular dataset from LIBSVM data
    # Datasets can be downloaded from LIBSVM Data: https://www.csie.ntu.edu.tw/~cjlin/libsvmtools/datasets/
    CLASSIFICATION_PATH = os.path.join("dataset", "binary_classification")
    REGRESSION_PATH = os.path.join("dataset", "regression")

    if not os.path.exists(CLASSIFICATION_PATH):
        os.makedirs(CLASSIFICATION_PATH)
        urllib.request.urlretrieve(
            "https://www.csie.ntu.edu.tw/~cjlin/libsvmtools/datasets/binary/cod-rna",
            f"{os.path.join(CLASSIFICATION_PATH, 'cod-rna')}",
        )
        urllib.request.urlretrieve(
            "https://www.csie.ntu.edu.tw/~cjlin/libsvmtools/datasets/binary/cod-rna.t",
            f"{os.path.join(CLASSIFICATION_PATH, 'cod-rna.t')}",
        )
        urllib.request.urlretrieve(
            "https://www.csie.ntu.edu.tw/~cjlin/libsvmtools/datasets/binary/cod-rna.r",
            f"{os.path.join(CLASSIFICATION_PATH, 'cod-rna.r')}",
        )
        urllib.request.urlretrieve(
            "https://www.csie.ntu.edu.tw/~cjlin/libsvmtools/datasets/binary/ijcnn1.t.bz2",
            f"{os.path.join(CLASSIFICATION_PATH, 'ijcnn1.t.bz2')}",
        )
        urllib.request.urlretrieve(
            "https://www.csie.ntu.edu.tw/~cjlin/libsvmtools/datasets/binary/ijcnn1.tr.bz2",
            f"{os.path.join(CLASSIFICATION_PATH, 'ijcnn1.tr.bz2')}",
        )
        for filepath in os.listdir(CLASSIFICATION_PATH):
            if filepath[-3:] == "bz2":
                abs_filepath = os.path.join(CLASSIFICATION_PATH, filepath)
                with bz2.BZ2File(abs_filepath) as fr, open(
                    abs_filepath[:-4], "wb"
                ) as fw:
                    shutil.copyfileobj(fr, fw)

    if not os.path.exists(REGRESSION_PATH):
        os.makedirs(REGRESSION_PATH)
        urllib.request.urlretrieve(
            "https://www.csie.ntu.edu.tw/~cjlin/libsvmtools/datasets/regression/eunite2001",
            f"{os.path.join(REGRESSION_PATH, 'eunite2001')}",
        )
        urllib.request.urlretrieve(
            "https://www.csie.ntu.edu.tw/~cjlin/libsvmtools/datasets/regression/eunite2001.t",
            f"{os.path.join(REGRESSION_PATH, 'eunite2001.t')}",
        )
        urllib.request.urlretrieve(
            "https://www.csie.ntu.edu.tw/~cjlin/libsvmtools/datasets/regression/YearPredictionMSD.bz2",
            f"{os.path.join(REGRESSION_PATH, 'YearPredictionMSD.bz2')}",
        )
        urllib.request.urlretrieve(
            "https://www.csie.ntu.edu.tw/~cjlin/libsvmtools/datasets/regression/YearPredictionMSD.t.bz2",
            f"{os.path.join(REGRESSION_PATH, 'YearPredictionMSD.t.bz2')}",
        )
        for filepath in os.listdir(REGRESSION_PATH):
            if filepath[-3:] == "bz2":
                abs_filepath = os.path.join(REGRESSION_PATH, filepath)
                with bz2.BZ2File(abs_filepath) as fr, open(
                    abs_filepath[:-4], "wb"
                ) as fw:
                    shutil.copyfileobj(fr, fw)

    binary_train = ["cod-rna.t", "cod-rna", "ijcnn1.t"]
    binary_test = ["cod-rna.r", "cod-rna.t", "ijcnn1.tr"]
    reg_train = ["eunite2001", "YearPredictionMSD"]
    reg_test = ["eunite2001.t", "YearPredictionMSD.t"]

    # Select the downloaded training and test dataset
    if task_type == "BINARY":
        dataset_path = "dataset/binary_classification/"
        train = binary_train[0]
        test = binary_test[0]
    elif task_type == "REG":
        dataset_path = "dataset/regression/"
        train = reg_train[0]
        test = reg_test[0]

    data_train = load_svmlight_file(dataset_path + train, zero_based=False)
    data_test = load_svmlight_file(dataset_path + test, zero_based=False)

    print("Task type selected is: " + task_type)
    print("Training dataset is: " + train)
    print("Test dataset is: " + test)

    X_train = data_train[0].toarray()
    y_train = data_train[1]
    X_test = data_test[0].toarray()
    y_test = data_test[1]

    if task_type == "BINARY":
        y_train[y_train == -1] = 0
        y_test[y_test == -1] = 0

    num_clients = config["num_clients"]

    if center_id != None:
        trainset = TreeDataset(
            np.array(X_train, copy=True), np.array(y_train, copy=True)
        )
        testset = TreeDataset(np.array(X_test, copy=True), np.array(y_test, copy=True))
        trainloaders, valloaders, testloader = do_fl_partitioning(
            trainset,
            testset,
            batch_size="whole",
            pool_size=num_clients,
            val_ratio=0.0,
        )
        X_train, y_train = [], []
        print(f"ID: {center_id}")
        for sample in trainloaders[center_id - 1]:
            X_train.extend(sample[0].numpy())
            y_train.extend(sample[1].numpy())
            # y_train.extend(sample[1].numpy()/2.0 + 0.5)

        # X_test, y_test = [], []
        # for sample in valloaders[center_id-1]:
        #     X_test.extend(sample[0].numpy())
        #     y_test.extend(sample[1].numpy()/2.0 + 0.5)

        # print(len(X_train))
        # print(len(y_train))
        # print(X_train[0])
        # print(y_train)
        X_train = np.array(X_train)
        y_train = np.array(y_train)
        # print(X_train.shape)
        # print(y_train.shape)

    train_unique = np.unique(y_train, return_counts=True)
    test_unique = np.unique(y_test, return_counts=True)
    # print(np.unique(y_train, return_counts=True))
    # print(np.unique(y_test, return_counts=True))
    train_max_acc = train_unique[1][0] / len(y_train)
    test_max_acc = test_unique[1][0] / len(y_test)
    # print(train_max_acc)
    # print(test_max_acc)

    return (X_train, y_train), (X_test, y_test)


def load_dataset(config, id=None):
    if config["dataset"] == "mnist":
        return load_mnist(id, config["num_clients"])
    elif config["dataset"] == "cvd":
        return load_cvd(config["data_path"], id)
    elif config["dataset"] == "kaggle_hf":
        return load_kaggle_hf(config["data_path"], id)
    elif config["dataset"] == "libsvm":
        return load_libsvm(config, id)
    else:
        raise ValueError("Invalid dataset name")

def get_stratifiedPartitions(n_splits,test_size, random_state):
    sss = StratifiedShuffleSplit(n_splits=n_splits,test_size=test_size, random_state=random_state)
    return sss

def split_partitions(n_splits,test_size, random_state,X_data, y_data):
    sss = get_stratifiedPartitions(n_splits,test_size, random_state)
    splits_nested = (sss.split(X_data, y_data))
    return splits_nested